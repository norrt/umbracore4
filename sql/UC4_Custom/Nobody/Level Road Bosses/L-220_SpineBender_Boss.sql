REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (200200, 0, 0, 0, 0, 0, 24787, 0, 0, 0, 'Spine Bender', 'Mordriaks Personal Guard', '', 0, 82, 82, 2, 16, 16, 0, 1, 1.5, 4, 3, 2000, 2480, 0, 300, 6, 2000, 0, 1, 0, 2048, 8, 0, 0, 0, 0, 0, 21, 30, 4, 4, 0, 0, 0, 0, 50, 50, 50, 50, 50, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5000000, 5000000, 'SmartAI', 0, 1, 1, 115, 10, 5, 0, 0, 0, 0, 0, 0, 0, 0, 1, 618348543, 0, '', 12340);

DELETE FROM `creature_text` WHERE `entry`=200200;
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (200200,1,0,'Begin ->Phase 1',41,0,100,1,0,17459,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (200200,2,0,'Phase 2',41,0,100,1,0,17459,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (200200,3,0,'Phase 3',41,0,100,1,0,17459,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (200200,4,0,'I will bend your bones and break your will!',16,0,100,1,0,0,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (200200,5,0,'My life for Mordriak!!!',16,0,100,1,0,0,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (200200,6,0,'You....will.....All....Die!!!',16,0,100,1,0,0,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (200200,7,0,'You are not prepared little mortals!',16,0,100,1,0,0,'Comment');
REPLACE INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (200200,8,0,'Curious....very curious indeed!!!',16,0,100,1,0,0,'Comment');

-- Change Entry ID
SET @ENTRY := 200200;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
REPLACE INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,1,0,2,0,100,1,95,100,0,0,11,2565,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Shield Block"),
(@ENTRY,@SOURCETYPE,2,0,2,0,100,0,90,95,15000,19000,11,70337,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Necrotic Plague"),
(@ENTRY,@SOURCETYPE,3,0,2,0,100,0,85,90,15000,19000,11,72133,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Pain and Suffering"),
(@ENTRY,@SOURCETYPE,4,0,2,0,100,0,80,85,4000,6000,11,64590,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Shield_breaker"),
(@ENTRY,@SOURCETYPE,5,0,2,0,100,1,75,80,0,0,11,40599,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Arching_Smash"),
(@ENTRY,@SOURCETYPE,6,0,2,0,100,0,70,75,15000,19000,11,72264,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Delirious Slash"),
(@ENTRY,@SOURCETYPE,7,0,2,0,100,0,65,70,13000,15000,11,59709,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Dark_Smash"),
(@ENTRY,@SOURCETYPE,8,0,2,0,100,0,60,65,10000,13000,11,52316,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Head Smash"),
(@ENTRY,@SOURCETYPE,9,0,2,0,100,0,55,60,15000,20000,11,59346,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Smash"),
(@ENTRY,@SOURCETYPE,10,0,2,0,100,0,50,55,13000,15000,11,64715,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Overhead_Smash"),
(@ENTRY,@SOURCETYPE,11,0,2,0,100,0,45,50,10000,13000,11,71077,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Tail Smash"),
(@ENTRY,@SOURCETYPE,12,0,2,0,100,0,40,45,13000,15000,11,59971,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Rain of Fire"),
(@ENTRY,@SOURCETYPE,13,0,2,0,100,0,35,40,13000,15000,11,67623,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Burning_Bile"),
(@ENTRY,@SOURCETYPE,14,0,2,0,100,0,30,35,15000,20000,11,40585,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Dark Barrage"),
(@ENTRY,@SOURCETYPE,15,0,2,0,100,0,25,30,5000,7000,11,39329,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Void Bolt"),
(@ENTRY,@SOURCETYPE,16,0,2,0,100,0,15,20,5000,7000,11,72170,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Flamestrike"),
(@ENTRY,@SOURCETYPE,17,0,2,0,100,0,10,15,5000,8000,11,30383,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Hateful Bolt"),
(@ENTRY,@SOURCETYPE,18,0,2,0,100,0,5,10,10000,15000,11,46240,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Earthquake"),
(@ENTRY,@SOURCETYPE,19,0,2,0,100,1,0,5,0,0,11,47008,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Berserk"),
(@ENTRY,@SOURCETYPE,20,0,2,0,100,1,60,100,0,0,1,1,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Phase 1"),
(@ENTRY,@SOURCETYPE,21,0,2,0,100,1,30,60,0,0,1,2,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Phase 2"),
(@ENTRY,@SOURCETYPE,22,0,2,0,100,1,1,30,0,0,1,3,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Phase 3"),
(@ENTRY,@SOURCETYPE,23,0,2,0,100,1,75,95,0,0,1,4,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"YELL4"),
(@ENTRY,@SOURCETYPE,24,0,2,0,100,1,55,75,0,0,1,5,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 5"),
(@ENTRY,@SOURCETYPE,25,0,2,0,100,1,35,55,0,0,1,6,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 6"),
(@ENTRY,@SOURCETYPE,26,0,2,0,100,1,15,35,0,0,1,7,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 7"),
(@ENTRY,@SOURCETYPE,27,0,2,0,100,1,1,15,0,0,1,8,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 8");