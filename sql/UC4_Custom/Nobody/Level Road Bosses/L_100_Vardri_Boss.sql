REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (100500, 0, 0, 0, 0, 0, 24942, 0, 0, 0, 'Vardri The Fallen', 'Doom Bringer', '', 0, 82, 82, 2, 16, 16, 0, 1, 1.5, 6, 3, 2000, 2480, 0, 300, 6, 2000, 0, 1, 0, 2048, 8, 0, 0, 0, 0, 0, 21, 30, 4, 4, 0, 0, 0, 0, 50, 50, 50, 50, 50, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5000000, 5000000, 'SmartAI', 0, 1, 1, 115, 10, 5, 0, 0, 0, 0, 0, 0, 0, 0, 1, 618348543, 0, '', 12340);

DELETE FROM `creature_text` WHERE `entry`=100500;
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100500,1,0,'You Dare Challenge ME!!! I will crush you souls foolish mortals!!!',14,0,100,1,0,17457,'Comment');
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100500,2,0,'Death will comsume you all!!!',14,0,100,1,0,17458,'Comment');
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100500,3,0,'DOOM! DOOOM! Can you feel it encompas you?',14,0,100,1,0,17459,'Comment');
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100500,4,0,'No!  The grave cannot keep me!  I will rise agian!!!',14,0,100,1,0,17460,'Comment');
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100500,5,0,'Phase 1',41,0,100,1,0,0,'Comment');
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES (100500,6,0,'Phase 2',41,0,100,1,0,0,'Comment');


SET @ENTRY := 100500;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,4,0,100,0,0,0,0,0,20,1,0,0,0,0,0,7,0,0,0,0.0,0.0,0.0,0.0,"On aggro do auto-attack."),
(@ENTRY,@SOURCETYPE,1,0,4,0,100,1,0,0,0,0,11,48266,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"On aggro cast Blood Presence(48266)."),
(@ENTRY,@SOURCETYPE,2,0,4,0,100,1,0,0,0,0,11,55222,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"On aggro cast Unholy Presence(55222)."),
(@ENTRY,@SOURCETYPE,3,0,0,0,100,0,60000,60000,60000,60000,11,49005,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Mark of Blood(49005) every 60 seconds."),
(@ENTRY,@SOURCETYPE,4,0,0,0,100,0,4000,4000,4000,4000,11,55978,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Heart Strike(55978) every 4 seconds."),
(@ENTRY,@SOURCETYPE,5,0,0,0,100,0,12000,12000,12000,12000,11,53639,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Death Strike(53639) every 12 seconds."),
(@ENTRY,@SOURCETYPE,6,0,0,0,100,0,6000,6000,6000,6000,11,66019,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Death Coil(66019) every 6 seconds."),
(@ENTRY,@SOURCETYPE,7,0,0,0,100,0,45000,45000,45000,45000,11,49203,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Hungering Cold(49203) every 45 seconds."),
(@ENTRY,@SOURCETYPE,8,0,0,0,100,0,46000,46000,46000,46000,11,61061,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Howling Blast(61061) every 46 seconds."),
(@ENTRY,@SOURCETYPE,9,0,0,0,100,0,30000,30000,30000,30000,11,72434,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Obliterate(72434) every 30 seconds."),
(@ENTRY,@SOURCETYPE,10,0,13,0,100,0,1,1,0,0,11,51052,2,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Anti-Magic Zone(51052) when target is casting."),
(@ENTRY,@SOURCETYPE,11,0,0,0,100,0,10000,10000,10000,10000,11,56359,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Death and Decay(56359) every 10 seconds."),
(@ENTRY,@SOURCETYPE,12,0,2,0,100,1,98,99,1,1,12,5,4,10000,0,1,0,5,0,0,0,0.0,0.0,0.0,0.0,"On aggro summon Deatchill Servant(5)."),
(@ENTRY,@SOURCETYPE,13,0,0,0,100,0,30000,30000,30000,30000,11,49206,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Summon Gargoyle(49206) every 30 seconds."),
(@ENTRY,@SOURCETYPE,14,0,6,0,100,1,0,0,0,0,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script on death."),
(@ENTRY,@SOURCETYPE,15,0,1,0,100,1,5000,5000,5000,5000,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script when out of combat."),
(@ENTRY,@SOURCETYPE,16,0,2,0,100,1,60,95,0,0,1,1,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 1"),
(@ENTRY,@SOURCETYPE,17,0,2,0,100,1,40,55,0,0,1,2,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 2"),
(@ENTRY,@SOURCETYPE,18,0,2,0,100,1,20,30,0,0,1,3,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 3"),
(@ENTRY,@SOURCETYPE,19,0,2,0,100,1,1,5,0,0,1,4,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Yell 4"),
(@ENTRY,@SOURCETYPE,20,0,2,0,100,1,55,100,0,0,1,5,1,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Emote 5"),
(@ENTRY,@SOURCETYPE,21,0,2,0,100,1,1,50,0,0,1,6,1,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Emote 6");






