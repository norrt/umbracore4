-- Beaky
SET @ENTRY := 4;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,0,0,100,0,15000,15000,15000,15000,11,39587,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Casting Wind Shock(39587) every 15 seconds."),
(@ENTRY,@SOURCETYPE,1,0,0,0,100,0,7000,7000,7000,7000,11,54588,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Casting Gust of Wind(54588) every 7 seconds"),
(@ENTRY,@SOURCETYPE,2,0,0,0,100,0,6000,6000,6000,6000,11,52814,4,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Arctic Winds(52814) every 6 seconds."),
(@ENTRY,@SOURCETYPE,3,0,0,0,100,0,20000,20000,20000,20000,11,38110,4,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Wing Buffet(38110) every 20 seconds."),
(@ENTRY,@SOURCETYPE,4,0,0,0,100,0,4000,4000,4000,4000,11,51877,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Coldwind Blast(51877) every 4 seconds.");