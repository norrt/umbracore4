-- -------------------------------------------------------- 
-- Level Road_ Boss Cragnor.sql 
-- -------------------------------------------------------- 

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/

SET @SMALL := (SELECT id+1 FROM gossip_menu_option WHERE menu_id='61011' ORDER BY id DESC LIMIT 1)+1;

INSERT INTO gossip_menu_option (menu_id, id, option_icon, option_text, option_id, npc_option_npcflag, action_menu_id, box_money, box_text) VALUES
('61011', @SMALL, '9', 'Level Road: Boss Cragnor', 1, 1, '61011', '0', 'Are you sure?');

SET @SID := (SELECT id FROM smart_scripts WHERE entryorguid = '29726' ORDER BY id DESC LIMIT 1)+1;
INSERT INTO smart_scripts (entryorguid, source_type, id, link, event_type, event_phase_mask, event_chance, event_flags, event_param1, event_param2, event_param3, event_param4, action_type, action_param1, action_param2, action_param3, action_param4, action_param5, action_param6, target_type, target_param1, target_param2, target_param3, target_x, target_y, target_z, target_o, comment) VALUES 
('29726', 0, IFNULL(@SID, 1), 0, 62, 0, 100, 0, '61011', @SMALL, 0, 0, 62, '1', 0, 0, 0, 0, 0, 0, 8, 0, 0, '5182.709', '-1300.189', '1364.979', '4.117', 'Teleporter script - Custom');

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/
 
 
-- -------------------------------------------------------- 
-- Level Road_ Boss Frizolial.sql 
-- -------------------------------------------------------- 

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/

SET @SMALL := (SELECT id+1 FROM gossip_menu_option WHERE menu_id='61011' ORDER BY id DESC LIMIT 1)+1;

INSERT INTO gossip_menu_option (menu_id, id, option_icon, option_text, option_id, npc_option_npcflag, action_menu_id, box_money, box_text) VALUES
('61011', @SMALL, '9', 'Level Road: Boss Frizolial', 1, 1, '61011', '0', 'Are you sure?');

SET @SID := (SELECT id FROM smart_scripts WHERE entryorguid = '29726' ORDER BY id DESC LIMIT 1)+1;
INSERT INTO smart_scripts (entryorguid, source_type, id, link, event_type, event_phase_mask, event_chance, event_flags, event_param1, event_param2, event_param3, event_param4, action_type, action_param1, action_param2, action_param3, action_param4, action_param5, action_param6, target_type, target_param1, target_param2, target_param3, target_x, target_y, target_z, target_o, comment) VALUES 
('29726', 0, IFNULL(@SID, 1), 0, 62, 0, 100, 0, '61011', @SMALL, 0, 0, 62, '1', 0, 0, 0, 0, 0, 0, 8, 0, 0, '4703.271', '-1750.803', '1146.978', '0.140', 'Teleporter script - Custom');

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/
 
 
-- -------------------------------------------------------- 
-- Level Road_ Boss Ignial.sql 
-- -------------------------------------------------------- 

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/

SET @SMALL := (SELECT id+1 FROM gossip_menu_option WHERE menu_id='61011' ORDER BY id DESC LIMIT 1)+1;

INSERT INTO gossip_menu_option (menu_id, id, option_icon, option_text, option_id, npc_option_npcflag, action_menu_id, box_money, box_text) VALUES
('61011', @SMALL, '9', 'Level Road: Boss Ignial', 1, 1, '61011', '0', 'Are you sure?');

SET @SID := (SELECT id FROM smart_scripts WHERE entryorguid = '29726' ORDER BY id DESC LIMIT 1)+1;
INSERT INTO smart_scripts (entryorguid, source_type, id, link, event_type, event_phase_mask, event_chance, event_flags, event_param1, event_param2, event_param3, event_param4, action_type, action_param1, action_param2, action_param3, action_param4, action_param5, action_param6, target_type, target_param1, target_param2, target_param3, target_x, target_y, target_z, target_o, comment) VALUES 
('29726', 0, IFNULL(@SID, 1), 0, 62, 0, 100, 0, '61011', @SMALL, 0, 0, 62, '1', 0, 0, 0, 0, 0, 0, 8, 0, 0, '4473.657', '-2512.034', '1127.894', '1.416', 'Teleporter script - Custom');

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/
 
 
-- -------------------------------------------------------- 
-- Level Road_ Boss Mordac.sql 
-- -------------------------------------------------------- 

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/

SET @SMALL := (SELECT id+1 FROM gossip_menu_option WHERE menu_id='61011' ORDER BY id DESC LIMIT 1)+1;

INSERT INTO gossip_menu_option (menu_id, id, option_icon, option_text, option_id, npc_option_npcflag, action_menu_id, box_money, box_text) VALUES
('61011', @SMALL, '9', 'Level Road: Boss Mordac', 1, 1, '61011', '0', 'Are you sure?');

SET @SID := (SELECT id FROM smart_scripts WHERE entryorguid = '29726' ORDER BY id DESC LIMIT 1)+1;
INSERT INTO smart_scripts (entryorguid, source_type, id, link, event_type, event_phase_mask, event_chance, event_flags, event_param1, event_param2, event_param3, event_param4, action_type, action_param1, action_param2, action_param3, action_param4, action_param5, action_param6, target_type, target_param1, target_param2, target_param3, target_x, target_y, target_z, target_o, comment) VALUES 
('29726', 0, IFNULL(@SID, 1), 0, 62, 0, 100, 0, '61011', @SMALL, 0, 0, 62, '1', 0, 0, 0, 0, 0, 0, 8, 0, 0, '5078.970', '-1801.959', '1327.439', '5.76', 'Teleporter script - Custom');

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/
 
 
-- -------------------------------------------------------- 
-- Level Road_ Boss Sitheros.sql 
-- -------------------------------------------------------- 

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/

SET
@BIG := (SELECT id FROM gossip_menu_option WHERE menu_id='61011' ORDER BY id DESC LIMIT 1),
@SMALL := (SELECT id FROM gossip_menu_option WHERE menu_id='61011' ORDER BY id ASC LIMIT 1),
@START := 1 + 1;

UPDATE gossip_menu_option
SET id = @BIG + id + @START
WHERE menu_id='61011';

UPDATE gossip_menu_option
SET id = id - @BIG - 1
WHERE menu_id='61011';

UPDATE conditions
SET SourceEntry = @BIG + SourceEntry + @START
WHERE SourceTypeOrReferenceId = 15 and SourceGroup = '61011';

UPDATE conditions
SET SourceEntry = SourceEntry - @BIG - 1
WHERE SourceTypeOrReferenceId = 15 and SourceGroup = '61011';

UPDATE smart_scripts
SET event_param2 = @BIG + event_param2 + @START
WHERE entryorguid='29726' AND event_param1 = '61011';

UPDATE smart_scripts
SET event_param2 = event_param2 - @BIG - 1
WHERE entryorguid='29726' AND event_param1 = '61011';


INSERT INTO gossip_menu_option (menu_id, id, option_icon, option_text, option_id, npc_option_npcflag, action_menu_id, box_money, box_text) VALUES
('61011', @SMALL, '9', 'Level Road: Boss Sitheros', 1, 1, '61011', '0', 'Are you sure?');

SET @SID := (SELECT id FROM smart_scripts WHERE entryorguid = '29726' ORDER BY id DESC LIMIT 1)+1;
INSERT INTO smart_scripts (entryorguid, source_type, id, link, event_type, event_phase_mask, event_chance, event_flags, event_param1, event_param2, event_param3, event_param4, action_type, action_param1, action_param2, action_param3, action_param4, action_param5, action_param6, target_type, target_param1, target_param2, target_param3, target_x, target_y, target_z, target_o, comment) VALUES 
('29726', 0, IFNULL(@SID, 1), 0, 62, 0, 100, 0, '61011', @SMALL, 0, 0, 62, '1', 0, 0, 0, 0, 0, 0, 8, 0, 0, '4519.271', '-3372.804', '995.567', '1.294', 'Teleporter script - Custom');

DELETE FROM conditions WHERE SourceTypeOrReferenceId=15 AND SourceGroup='61011' AND SourceEntry=@BIG+1;

INSERT INTO conditions (SourceTypeOrReferenceId, SourceGroup, SourceEntry, ConditionTypeOrReference, ConditionValue1, ConditionValue2, ConditionValue3, Comment) VALUES
(15, '61011', @SMALL, 27, '15', 3, 0, 'Portal Master - Custom Level req');

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/
 
 
-- -------------------------------------------------------- 
-- Level Road_ Boss Vardri.sql 
-- -------------------------------------------------------- 

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/

SET @SMALL := (SELECT id+1 FROM gossip_menu_option WHERE menu_id='61011' ORDER BY id DESC LIMIT 1)+1;

INSERT INTO gossip_menu_option (menu_id, id, option_icon, option_text, option_id, npc_option_npcflag, action_menu_id, box_money, box_text) VALUES
('61011', @SMALL, '9', 'Level Road: Boss Vardri', 1, 1, '61011', '0', 'Are you sure?');

SET @SID := (SELECT id FROM smart_scripts WHERE entryorguid = '29726' ORDER BY id DESC LIMIT 1)+1;
INSERT INTO smart_scripts (entryorguid, source_type, id, link, event_type, event_phase_mask, event_chance, event_flags, event_param1, event_param2, event_param3, event_param4, action_type, action_param1, action_param2, action_param3, action_param4, action_param5, action_param6, target_type, target_param1, target_param2, target_param3, target_x, target_y, target_z, target_o, comment) VALUES 
('29726', 0, IFNULL(@SID, 1), 0, 62, 0, 100, 0, '61011', @SMALL, 0, 0, 62, '1', 0, 0, 0, 0, 0, 0, 8, 0, 0, '5269.189', '-1522.109', '1363.260', '2.280', 'Teleporter script - Custom');

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/
 
 
-- -------------------------------------------------------- 
-- Level Road_ Boss Zalistra.sql 
-- -------------------------------------------------------- 

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/

SET @SMALL := (SELECT id+1 FROM gossip_menu_option WHERE menu_id='61011' ORDER BY id DESC LIMIT 1)+1;

INSERT INTO gossip_menu_option (menu_id, id, option_icon, option_text, option_id, npc_option_npcflag, action_menu_id, box_money, box_text) VALUES
('61011', @SMALL, '9', 'Level Road: Boss Zalistra', 1, 1, '61011', '0', 'Are you sure?');

SET @SID := (SELECT id FROM smart_scripts WHERE entryorguid = '29726' ORDER BY id DESC LIMIT 1)+1;
INSERT INTO smart_scripts (entryorguid, source_type, id, link, event_type, event_phase_mask, event_chance, event_flags, event_param1, event_param2, event_param3, event_param4, action_type, action_param1, action_param2, action_param3, action_param4, action_param5, action_param6, target_type, target_param1, target_param2, target_param3, target_x, target_y, target_z, target_o, comment) VALUES 
('29726', 0, IFNULL(@SID, 1), 0, 62, 0, 100, 0, '61011', @SMALL, 0, 0, 62, '1', 0, 0, 0, 0, 0, 0, 8, 0, 0, '5264.509', '-2158.510', '1259.329', '4.720', 'Teleporter script - Custom');

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/
 
 
-- -------------------------------------------------------- 
-- Level Road_ Start.sql 
-- -------------------------------------------------------- 

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/

SET
@BIG := (SELECT id FROM gossip_menu_option WHERE menu_id='61011' ORDER BY id DESC LIMIT 1),
@SMALL := (SELECT id FROM gossip_menu_option WHERE menu_id='61011' ORDER BY id ASC LIMIT 1),
@START := 1 + 1;

UPDATE gossip_menu_option
SET id = @BIG + id + @START
WHERE menu_id='61011';

UPDATE gossip_menu_option
SET id = id - @BIG - 1
WHERE menu_id='61011';

UPDATE conditions
SET SourceEntry = @BIG + SourceEntry + @START
WHERE SourceTypeOrReferenceId = 15 and SourceGroup = '61011';

UPDATE conditions
SET SourceEntry = SourceEntry - @BIG - 1
WHERE SourceTypeOrReferenceId = 15 and SourceGroup = '61011';

UPDATE smart_scripts
SET event_param2 = @BIG + event_param2 + @START
WHERE entryorguid='29726' AND event_param1 = '61011';

UPDATE smart_scripts
SET event_param2 = event_param2 - @BIG - 1
WHERE entryorguid='29726' AND event_param1 = '61011';


INSERT INTO gossip_menu_option (menu_id, id, option_icon, option_text, option_id, npc_option_npcflag, action_menu_id, box_money, box_text) VALUES
('61011', @SMALL, '0', 'Level Road: Start', 1, 1, '61011', '0', 'Are you sure?');

SET @SID := (SELECT id FROM smart_scripts WHERE entryorguid = '29726' ORDER BY id DESC LIMIT 1)+1;
INSERT INTO smart_scripts (entryorguid, source_type, id, link, event_type, event_phase_mask, event_chance, event_flags, event_param1, event_param2, event_param3, event_param4, action_type, action_param1, action_param2, action_param3, action_param4, action_param5, action_param6, target_type, target_param1, target_param2, target_param3, target_x, target_y, target_z, target_o, comment) VALUES 
('29726', 0, IFNULL(@SID, 1), 0, 62, 0, 100, 0, '61011', @SMALL, 0, 0, 62, '1', 0, 0, 0, 0, 0, 0, 8, 0, 0, '4611.390', '-3864.030', '944.184', '1.069', 'Teleporter script - Custom');

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/
 
 
-- -------------------------------------------------------- 
-- Ragefire Instance_ Level 10.sql 
-- -------------------------------------------------------- 

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/

SET
@BIG := (SELECT id FROM gossip_menu_option WHERE menu_id='61011' ORDER BY id DESC LIMIT 1),
@SMALL := (SELECT id FROM gossip_menu_option WHERE menu_id='61011' ORDER BY id ASC LIMIT 1),
@START := 1 + 1;

UPDATE gossip_menu_option
SET id = @BIG + id + @START
WHERE menu_id='61011';

UPDATE gossip_menu_option
SET id = id - @BIG - 1
WHERE menu_id='61011';

UPDATE conditions
SET SourceEntry = @BIG + SourceEntry + @START
WHERE SourceTypeOrReferenceId = 15 and SourceGroup = '61011';

UPDATE conditions
SET SourceEntry = SourceEntry - @BIG - 1
WHERE SourceTypeOrReferenceId = 15 and SourceGroup = '61011';

UPDATE smart_scripts
SET event_param2 = @BIG + event_param2 + @START
WHERE entryorguid='29726' AND event_param1 = '61011';

UPDATE smart_scripts
SET event_param2 = event_param2 - @BIG - 1
WHERE entryorguid='29726' AND event_param1 = '61011';


INSERT INTO gossip_menu_option (menu_id, id, option_icon, option_text, option_id, npc_option_npcflag, action_menu_id, box_money, box_text) VALUES
('61011', @SMALL, '9', 'Ragefire Instance: Level 10', 1, 1, '61011', '0', 'Are you sure?');

SET @SID := (SELECT id FROM smart_scripts WHERE entryorguid = '29726' ORDER BY id DESC LIMIT 1)+1;
INSERT INTO smart_scripts (entryorguid, source_type, id, link, event_type, event_phase_mask, event_chance, event_flags, event_param1, event_param2, event_param3, event_param4, action_type, action_param1, action_param2, action_param3, action_param4, action_param5, action_param6, target_type, target_param1, target_param2, target_param3, target_x, target_y, target_z, target_o, comment) VALUES 
('29726', 0, IFNULL(@SID, 1), 0, 62, 0, 100, 0, '61011', @SMALL, 0, 0, 62, '1', 0, 0, 0, 0, 0, 0, 8, 0, 0, '1811.780', '-4410.500', '-18.469', '5.307', 'Teleporter script - Custom');

DELETE FROM conditions WHERE SourceTypeOrReferenceId=15 AND SourceGroup='61011' AND SourceEntry=@BIG+1;

INSERT INTO conditions (SourceTypeOrReferenceId, SourceGroup, SourceEntry, ConditionTypeOrReference, ConditionValue1, ConditionValue2, ConditionValue3, Comment) VALUES
(15, '61011', @SMALL, 27, '10', 3, 0, 'Portal Master - Custom Level req');

/*
Portal Master Option
By Rochet2
Downloaded from http://projectcode.zzl.org/
Bugs and contact with E-mail: Rochet2@post.com
*/
 
 
-- -------------------------------------------------------- 
-- _MERGED.sql 
-- -------------------------------------------------------- 
 
 
