-- Warrior
SET @ENTRY := 80002;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,1,0,4,0,100,0,0,0,0,0,11,11578,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"On aggro cast Charge(25821)."),
(@ENTRY,@SOURCETYPE,2,0,2,0,100,1,97,99,1,10,11,54708,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Casting Rend(54708) on start of battle."),
(@ENTRY,@SOURCETYPE,3,0,2,0,100,1,96,97,1,10,11,27584,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Cast Hamstring(27584)."),
(@ENTRY,@SOURCETYPE,4,0,24,0,100,1,27584,1,1,1,11,67541,1,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Cast Bladestorm(67541) when target has Hamstring(27584)."),
(@ENTRY,@SOURCETYPE,5,0,13,0,1,0,1,1,0,0,11,53394,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"(1% chance) When target is casting will Pummel(53394) target."),
(@ENTRY,@SOURCETYPE,6,0,0,0,100,0,20000,20000,20000,20000,11,62444,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Casting Heroic Strike(62444) every 20 seconds."),
(@ENTRY,@SOURCETYPE,7,0,0,0,100,0,50000,50000,50000,50000,11,54132,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Casting Concussion Blow(54132) every 50 seconds."),
(@ENTRY,@SOURCETYPE,8,0,0,0,100,0,15000,15000,15000,15000,11,15708,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Casting Mortal Strike(15708) every 15 seconds."),
(@ENTRY,@SOURCETYPE,9,0,0,0,100,0,5000,5000,5000,5000,11,30474,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Casting Bloodthirst(30474) every 5 seconds."),
(@ENTRY,@SOURCETYPE,10,0,2,0,100,1,48,50,1,1,11,65930,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 50% cast Intimidating Shout(65930)."),
(@ENTRY,@SOURCETYPE,11,0,2,0,100,1,43,45,1,1,11,6713,4,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"At 45% cast Disarm(6713)."),
(@ENTRY,@SOURCETYPE,12,0,2,0,100,1,35,37,0,0,11,53479,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"At 37% cast Last Stand(53479)."),
(@ENTRY,@SOURCETYPE,13,0,23,0,100,1,53479,1,1,1,11,15062,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"When He has Last Stand(53479) cast Shield Wall(15062)."),
(@ENTRY,@SOURCETYPE,14,0,2,0,100,1,23,25,1,1,11,75418,4,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"At 25% cast Shockwave(75418) on target."),
(@ENTRY,@SOURCETYPE,15,0,12,0,100,0,0,20,1,100,11,56426,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"When player has 20% hp casting Execute."),
(@ENTRY,@SOURCETYPE,16,0,6,0,100,1,0,0,0,0,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script on death."),
(@ENTRY,@SOURCETYPE,17,0,1,0,100,1,10000,10000,1,1,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script when out of combat."),
(@ENTRY,@SOURCETYPE,18,0,2,0,100,0,4,5,1,1,11,67541,0,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Cast Bladestorm(67541) at 5%.");