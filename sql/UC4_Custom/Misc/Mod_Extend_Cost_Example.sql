-- Set Extended Cost for NPC by Entry ID
SET
@ExtendedCost := 'Extended Cost ID',
@NPCENTRY := 'NPC Entry ID';

UPDATE `npc_vendor` SET `ExtendedCost`=@ExtendedCost WHERE `entry`=@NPCENTRY;