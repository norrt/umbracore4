#ifndef _PBOT_GRIDNOTIFIERS_H
#define _PBOT_GRIDNOTIFIERS_H

#include "Group.h"
#include "Player.h"
#include "SpellAuras.h"
#include "bp_ai.h"

class PBNearestHostileUnitCheck
{
public:
    explicit PBNearestHostileUnitCheck(Unit const* unit, float dist, uint32 dmgmask, bool targetCCed = false) : 
    me(unit->ToPlayer()), _range(dist), _mask(dmgmask), AttackCCed(targetCCed) { }
    bool operator()(Unit* u)
    {
        if (!me->IsWithinDistInMap(u, _range))
            return false;
        if (!u->isInCombat())
            return false;
        if (!PlayerbotAI::CanPlayerbotAttack(me, u, _mask))
            return false;
        if (PlayerbotAI::IsUnitInDuel(u, me->GetPlayerbotAI()->GetMaster()))
            return false;
        if (!AttackCCed &&
            (u->HasUnitState(UNIT_STATE_CONFUSED | UNIT_STATE_STUNNED | UNIT_STATE_FLEEING | UNIT_STATE_DISTRACTED | UNIT_STATE_CONFUSED_MOVE | UNIT_STATE_FLEEING_MOVE)))
            return false;
        if (!u->getVictim())
            return false;
        if (!PlayerbotAI::IsUnitInPlayersParty(u->getVictim(), me->GetPlayerbotAI()->GetMaster()))
            return false;

        _range = me->GetDistance(u);   // use found unit range as new range limit for next check
        return true;
    }
private:
    Player const* me;
    float _range;
    uint32 _mask;
    bool AttackCCed;
    PBNearestHostileUnitCheck(PBNearestHostileUnitCheck const&);
};



#endif
