-- Priest
SET @ENTRY := 80007;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,2,0,100,1,50,99,1,1,22,1,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: 100%-50%."),
(@ENTRY,@SOURCETYPE,1,0,0,1,100,0,9000,9000,9000,9000,11,66538,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: Holy Fire(66538) every 9 seconds."),
(@ENTRY,@SOURCETYPE,2,0,0,1,100,0,4000,4000,4000,4000,11,59703,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: Holy Smite(59703) every 4 seconds."),
(@ENTRY,@SOURCETYPE,3,0,0,1,100,0,14000,14000,14000,14000,11,57465,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: Holy Bolt(57465) every 14 seconds."),
(@ENTRY,@SOURCETYPE,4,0,2,1,100,0,98,99,1,1,11,46565,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: Holyform(46565)."),
(@ENTRY,@SOURCETYPE,5,0,0,1,100,0,12000,12000,12000,12000,11,60004,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: Renew(60004) every 12 seconds."),
(@ENTRY,@SOURCETYPE,7,0,0,1,100,0,30000,30000,30000,30000,11,66546,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Phase 1: Holy Nova(66546) every 30 seconds."),
(@ENTRY,@SOURCETYPE,8,0,2,0,100,1,50,51,1,1,28,46565,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Remove Holyform(46565) for phase switch."),
(@ENTRY,@SOURCETYPE,9,0,2,0,100,1,1,49,1,1,22,2,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: 50%-0%."),
(@ENTRY,@SOURCETYPE,10,0,2,2,100,1,48,49,1,1,11,35194,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: Shadowform(35194)."),
(@ENTRY,@SOURCETYPE,11,0,0,2,100,0,24000,24000,24000,24000,11,48300,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: Devouring Plague(48300) every 24 seconds."),
(@ENTRY,@SOURCETYPE,12,0,0,2,100,0,15000,15000,15000,15000,11,65541,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: Shadow Word:Pain(65541) every 15 seconds."),
(@ENTRY,@SOURCETYPE,13,0,0,2,100,0,16000,16000,16000,16000,11,65490,2,0,0,0,0,2,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: Vampiric Touch(65490) every 16 seconds."),
(@ENTRY,@SOURCETYPE,14,0,0,2,100,0,7000,7000,7000,7000,11,65492,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: Mind Blast(65492) every 7 seconds."),
(@ENTRY,@SOURCETYPE,15,0,0,2,100,0,4000,4000,4000,4000,11,65488,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Phase 2: Mind Flay(65488) every 4 seconds."),
(@ENTRY,@SOURCETYPE,16,0,1,0,100,1,2000,2000,2000,2000,28,35194,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Remove Shadowform(35194) when out of combat."),
(@ENTRY,@SOURCETYPE,17,0,0,0,100,0,60000,60000,60000,60000,11,26042,2,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"Psychic Scream(26042) every 60 seconds."),
(@ENTRY,@SOURCETYPE,18,0,2,0,100,1,1,1,1,1,11,47788,16,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Guardian Spirit when close to death."),
(@ENTRY,@SOURCETYPE,19,0,6,0,100,1,0,0,0,0,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script on death."),
(@ENTRY,@SOURCETYPE,20,0,1,0,100,1,5000,5000,5000,5000,78,0,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"Reset script when out of combat.");