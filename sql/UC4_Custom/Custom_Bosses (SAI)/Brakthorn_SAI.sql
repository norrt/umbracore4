--The Council of Three
--RageFire Chasm
--Boss Brakthorn's SAI SQL
--UmbraCore2 (C) 2013
--Nobody

SET @ENTRY := 800051;
SET @SOURCETYPE := 0;

DELETE FROM `smart_scripts` WHERE `entryorguid`=@ENTRY AND `source_type`=@SOURCETYPE;
UPDATE creature_template SET AIName="SmartAI" WHERE entry=@ENTRY LIMIT 1;
INSERT INTO `smart_scripts` (`entryorguid`,`source_type`,`id`,`link`,`event_type`,`event_phase_mask`,`event_chance`,`event_flags`,`event_param1`,`event_param2`,`event_param3`,`event_param4`,`action_type`,`action_param1`,`action_param2`,`action_param3`,`action_param4`,`action_param5`,`action_param6`,`target_type`,`target_param1`,`target_param2`,`target_param3`,`target_x`,`target_y`,`target_z`,`target_o`,`comment`) VALUES 
(@ENTRY,@SOURCETYPE,0,0,4,0,100,0,0,0,0,0,1,800051,0,0,0,0,0,0,0,0,0,0.0,0.0,0.0,0.0,"On Aggro -Talk"),
(@ENTRY,@SOURCETYPE,1,0,4,0,100,0,0,0,0,0,11,324,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"On Aggro - Buff1"),
(@ENTRY,@SOURCETYPE,2,0,4,0,100,0,0,0,0,0,11,467,0,0,0,0,0,1,0,0,0,0.0,0.0,0.0,0.0,"On Aggro - Buff2"),
(@ENTRY,@SOURCETYPE,3,0,4,0,100,0,0,0,0,0,11,403,0,0,0,0,0,5,0,0,0,0.0,0.0,0.0,0.0,"On Aggro - Lighting Bolt");