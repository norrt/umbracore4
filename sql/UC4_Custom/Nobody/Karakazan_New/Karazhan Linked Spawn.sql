-- Gizella ID: 255008 GUID: 15026614
-- Broodlings
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15021795, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15021793, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15021791, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15021787, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15021789, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15021785, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15021781, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15023042, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15026614, 15026614, 0);

-- Pre-Banquet
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024279, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024277, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15023048, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15023046, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024237, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024233, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024231, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024235, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024207, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024205, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024203, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024201, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024209, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024213, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024211, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024219, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024217, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024215, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024227, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024229, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024221, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024223, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024225, 15026614, 0);

-- Banquet Hall

REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024239, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024241, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024245, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024273, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024275, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024285, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024287, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024289, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024251, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15204247, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024249, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024253, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024255, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024257, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024259, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024261, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024263, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024243, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024265, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024269, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024267, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024271, 15026614, 0);

-- Grand Ballroom

REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024326, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024328, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024293, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024297, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024299, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024303, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024322, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024324, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024320, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024316, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024314, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024318, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024332, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024330, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024310, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024308, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024312, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024336, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024334, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024338, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024267, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024271, 15026614, 0);

-- Guest Chambers

REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024340, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15025472, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15025474, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024342, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15025476, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15025478, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024344, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15025482, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15025480, 15026614, 0);
REPLACE INTO `linked_respawn` (`guid`, `linkedGuid`, `linkType`) Values (15024346, 15026614, 0);